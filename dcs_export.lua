package.path = package.path..";.\\LuaSocket\\?.lua"
package.cpath = package.cpath..";\\LuaSocket\\?.dll"

socket = require("socket")
tcp = assert(socket.tcp())

json = loadfile("Scripts\\JSON.lua")()

--host = host or IP ADDRESS OF TCP LISTENER
--port = port or TCP LISTENER PORT

env.info("Trying to connect to local server")
tcp:connect(host, port)
env.info("TCP Connected!")

--serverId = SERVER ID
--serverId = 2
--missionName = "v8"
--missionName = "missionName"
sessionId = math.random(1000,9999)

heartbeat = {
    serverId = nil
}

playerTotals = {
    sessionId = nil,
    playerTotal = nil,
    blueTotal = nil,
    redTotal = nil
}

playerList = {
    serverId = nil,
    sessionId = nil,
    playerName = nil,
    playerCoalition = nil
}

serverStatus = {
    serverId = nil,
    sessionId = nil,
	host = nil,
	name = nil,
    mission = nil
}

playerData = {
    sessionId = nil,
    serverId = nil,
	playerName = nil,
    playerCoalition = nil,
    playerScore = nil,
    playerLastOnline = nil
}

airbossData = {
    playerName  = nil,
    pass = nil,
    pointsFinal = nil,
    pointsPass = nil,
    grade = nil,
    details = nil,
    wire = nil,
    tgroove = nil,
    landingCase = nil,
    wind = nil,
    modex = nil,
    airframe = nil,
    carrierType = nil,
    carrierName = nil,
    theatre = nil,
    missionTime = nil,
    missionDate = nil,
    osDate = nil
}

missionStats = {
    sessionId = nil,
    serverId = nil,
    mission = nil,
    playerJoins = nil,
    redUnitTotal = nil,
    blueUnitTotal = nil,
    unitTotal = nil,
    unitTotalDeaths = nil,
    redUnitTotalDeaths = nil,
    blueUnitTotalDeaths = nil,
    redAirbaseTotal = nil,
    blueAirbaseTotal = nil,
    redAirplaneUnitTotal = nil,
    blueAirplaneUnitTotal = nil,
    airplaneUnitTotal = nil,
    redGroundUnitTotal = nil,
    blueGroundUnitTotal = nil,
    groundUnitTotal = nil,
    redShipUnitTotal = nil,
    blueShipUnitTotal = nil,
    shipUnitTotal = nil,
    redHelicopterUnitTotal = nil,
    blueHelicopterUnitTotal = nil,
    helicopterUnitTotal = nil,
    playerTotalDeaths = nil
}

serverStatus.serverId = serverId
serverStatus.sessionId = sessionId
serverStatus.name = "sierrahotel-02"
serverStatus.host = "nolove.sierrahotel.io"
serverStatus.mission = missionName

jsonServerStatus = json:encode(serverStatus)
tcp:send("HS|"..jsonServerStatus.."\n")
socket.sleep(0.001)

-- Create Unit Sets For Mission Stats
redUnitSet = SET_UNIT:New():FilterCoalitions("red"):FilterStart()
blueUnitSet = SET_UNIT:New():FilterCoalitions("blue"):FilterStart()
allUnitSet = SET_UNIT:New():FilterStart()
allClientSet = SET_CLIENT:New():FilterStart()

redAirbaseSet = SET_AIRBASE:New():FilterCoalitions("red"):FilterStart()
blueAirbaseSet = SET_AIRBASE:New():FilterCoalitions("blue"):FilterStart()

local playerJoins = 0
local redUnitTotalDeaths = 0
local blueUnitTotalDeaths = 0    
local unitTotalDeaths = 0
local playerTotalDeaths = 0

allUnitSet:HandleEvent(EVENTS.Dead)

function allUnitSet:OnEventDead(EventData)
    
    env.info("Unit Killed")
    unitTotalDeaths = unitTotalDeaths + 1
    
    local unitDeadName = EventData.IniUnitName
    env.info("Unit: " .. unitDeadName .. " Is Dead!")
    
    local deadUnit = EventData.IniUnit
    local playerName = deadUnit:GetPlayerName()

    if playerName == nil then

        local deadUnitCoalition = deadUnit:GetCoalition()
        env.info("Dead Unit Coalition: " .. deadUnitCoalition)
    
        if deadUnitCoalition == 1 then
            redUnitTotalDeaths = redUnitTotalDeaths + 1
        else 
            blueUnitTotalDeaths = blueUnitTotalDeaths + 1
        end
    else 
        playerTotalDeaths = playerTotalDeaths + 1
        env.info("Player: " .. playerName .. " is dead!")
    end
    
    
end

--[[
allClientSet:HandleEvent(EVENTS.Birth)

function allClientSet:EventOnBirth(EventData)
    env.info("A client has spawned")
end
--]]

function getMissionStats()
    local redUnitTotal = 0
    local blueUnitTotal = 0
    local unitTotal = 0
    local redAirbaseTotal = 0
    local blueAirbaseTotal = 0
    local redAirplaneUnitTotal = 0
    local blueAirplaneUnitTotal = 0
    local airplaneUnitTotal = 0
    local redGroundUnitTotal = 0
    local blueGroundUnitTotal = 0
    local groundUnitTotal = 0
    local redShipUnitTotal = 0
    local blueShipUnitTotal = 0
    local shipUnitTotal = 0
    local redHelicopterUnitTotal = 0
    local blueHelicopterUnitTotal = 0
    local helicopterUnitTotal = 0

    redUnitTotal = redUnitSet:Count()
    blueUnitTotal = blueUnitSet:Count()
    unitTotal = redUnitTotal + blueUnitTotal

    redAirbaseTotal = redAirbaseSet:Count()
    blueAirbaseTotal = blueAirbaseSet:Count()

    local unitTable = {}

    allUnitSet:ForEachUnit(
        function(unit)
            if unit:IsAlive() == true then
                unitCategory = unit:GetCategoryName()
                unitCoalition = unit:GetCoalition()
                --env.info("Unit Category: " .. unitCategory)
            
                table.insert(unitTable, { category = unitCategory, coalition = unitCoalition })
            end

        end
    )

    --env.info("unitTable: " .. tableDump(unitTable))
    
    for unit, unitData in pairs(unitTable) do
        if unitData.category == "Ground Unit" then
            groundUnitTotal = groundUnitTotal + 1
            if unitData.coalition == 1 then
                redGroundUnitTotal = redGroundUnitTotal + 1
            else
                blueGroundUnitTotal = blueGroundUnitTotal + 1
            end
        elseif unitData.category == "Airplane" then
            airplaneUnitTotal = airplaneUnitTotal + 1
            if unitData.coalition == 1 then
                --env.info("Red Airplane Unit Found!")
                redAirplaneUnitTotal = redAirplaneUnitTotal + 1
            else
                --env.info("Blue Airplane Unit Found!")
                blueAirplaneUnitTotal = blueAirplaneUnitTotal + 1
            end
        elseif unitData.category == "Ship" then
            shipUnitTotal = shipUnitTotal + 1
            --env.info("Ship Unit Coalition: " .. unitData.coalition)
            if unitData.coalition == 1 then
                redShipUnitTotal = redShipUnitTotal + 1
            else
                blueShipUnitTotal = blueShipUnitTotal + 1
            end
        elseif unitData.category == "Helicopter" then
            helicopterUnitTotal = helicopterUnitTotal + 1
            if unitData.coalition == 1 then
                --env.info("Red Helicopter Unit Found!")
                redHelicopterUnitTotal = redHelicopterUnitTotal + 1
            else
                --env.info("Blue Helicopter Unit Found!")
                blueHelicopterUnitTotal = blueHelicopterUnitTotal + 1
            end
        end

    end

    return redUnitTotal, blueUnitTotal, unitTotal, redAirbaseTotal, blueAirbaseTotal, redAirplaneUnitTotal, blueAirplaneUnitTotal, airplaneUnitTotal, redGroundUnitTotal, blueGroundUnitTotal, groundUnitTotal, redShipUnitTotal, blueShipUnitTotal, shipUnitTotal, redHelicopterUnitTotal, blueHelicopterUnitTotal, helicopterUnitTotal
end

function AIRBOSS:onafterSave(From, Event, To, path, filename)
    env.info("Inside Airboss onafterSave event")
    for playername,grades in pairs(self.playerscores) do
  
        -- Loop over player grades table.
        for i,_grade in pairs(grades) do
            local grade=_grade --#AIRBOSS.LSOgrade
      
            -- Check some stuff that could be nil.
            local wire="n/a"
            if grade.wire and grade.wire<=4 then
                wire=tostring(grade.wire)
            end
      
            local Tgroove="n/a"
            if grade.Tgroove and grade.Tgroove<=360 and grade.case<3 then
                Tgroove=tostring(UTILS.Round(grade.Tgroove, 1))
            end
      
            local finalscore="n/a"
            if grade.finalscore then
                finalscore=tostring(UTILS.Round(grade.finalscore, 1))
            end
      
            airbossData.playerName = playername
            airbossData.pass = i
            airbossData.pointsFinal = finalscore
            airbossData.pointsPass = grade.points
            airbossData.grade = grade.grade
            airbossData.details = grade.details
            airbossData.wire = wire
            airbossData.tgroove = Tgroove
            airbossData.landingCase = grade.case
            airbossData.wind = grade.wind
            airbossData.modex = grade.modex
            airbossData.airframe = grade.airframe
            airbossData.carrierType = grade.carriertype
            airbossData.carrierName = grade.carriername
            airbossData.theatre = grade.theatre
            airbossData.mitime = grade.mitime
            airbossData.midate = grade.midate
            airbossData.osDate = grade.osdate
            
            jsonAirbossData = json:encode(airbossData)
            tcp:send("PA|" ..jsonAirbossData.."\n")
        end
    end
end

function client_count()
    local clients_SET = SET_CLIENT:New():FilterOnce()
    local player_count = 0
    local player_blue = 0
    local player_red = 0
    local players = {}

    clients_SET:ForEachClient( 
        function( MooseClient ) 
        if MooseClient:IsAlive() == true then
            local player = MooseClient:GetPlayerName()
            if player ~= nil then
                env.info("Player Name: " .. player)
                --players[#players + 1] = { playerName = player, playerCoalition = MooseClient:GetCoalition() }
                player_count = player_count + 1
                if MooseClient:GetCoalition() == coalition.side.BLUE then
                    player_blue = player_blue + 1
                    players[#players + 1] = { playerName = player, playerCoalition = "Blue" }
                else
                    player_red = player_red + 1
                    players[#players + 1] = { playerName = player, playerCoalition = "Red" }
                end
            end
        end
    end)

    t_coalition = { ["blue"] = player_blue, ["red"] = player_red}
    return player_count, t_coalition, players
end
--[[
local airbossTest = SCHEDULER:New( nil,
    function()
        env.info("Inside airbossTest")

        airbossData.playerName = "VOID â„¦ MavA"
        airbossData.pass = 1
        airbossData.pointsFinal = "n/a"
        airbossData.pointsPass = 2.5
        airbossData.grade = "=-- (BOLTER)"
        airbossData.details = "(F)_HLUL_X (SLO)_HLUL_IM  (SLO)_H_(LUR)IC (SLOLUR)AR"
        airbossData.wire = "n/a"
        airbossData.tgroove = "16.5"
        airbossData.landingCase = 1
        airbossData.wind = "n/a"
        airbossData.modex = "n/a"
        airbossData.airframe = "n/a"
        airbossData.carrierType = "n/a"
        airbossData.carrierName = "n/a"
        airbossData.theatre = "n/a"
        airbossData.mitime = "n/a"
        airbossData.midate = "n/a"
        airbossData.osDate = "n/a"

        jsonAirbossData = json:encode(airbossData)
        tcp:send("PA|" ..jsonAirbossData.."\n")

    end, {}, 10, 15
)
--]]

local missionStatsCheck = SCHEDULER:New( nil,
    function()
        redUnitTotal, blueUnitTotal, unitTotal, redAirbaseTotal, blueAirbaseTotal, redAirplaneUnitTotal, blueAirplaneUnitTotal, airplaneUnitTotal, redGroundUnitTotal, blueGroundUnitTotal, groundUnitTotal, redShipUnitTotal, blueShipUnitTotal, shipUnitTotal, redHelicopterUnitTotal, blueHelicopterUnitTotal, helicopterUnitTotal = getMissionStats()
        
        env.info("Red Helicopter Unit Total: " .. redHelicopterUnitTotal)
        env.info("Blue Helicopter Unit Total: " .. blueHelicopterUnitTotal)
        env.info("Helicopter Unit Total: " .. helicopterUnitTotal)

        env.info("Red Airplane Unit Total: " .. redAirplaneUnitTotal)
        env.info("Blue Airplane Unit Total: " .. blueAirplaneUnitTotal)
        env.info("Airplane Unit Total: " .. airplaneUnitTotal)
        
        env.info("Red Ship Unit Total: " .. redShipUnitTotal)
        env.info("Blue Ship Unit Total: " .. blueShipUnitTotal)
        env.info("Ship Unit Total: " .. shipUnitTotal)
        
        env.info("Red Unit Total Deaths: " .. redUnitTotalDeaths)
        env.info("Blue Unit Total Deaths: " .. blueUnitTotalDeaths)
        env.info("Total Unit Deaths: " .. unitTotalDeaths)

        missionStats.sessionId = sessionId
        missionStats.serverId = serverId
        missionStats.mission = missionName
        missionStats.playerJoins = playerJoins
        missionStats.redUnitTotal = redUnitTotal
        missionStats.blueUnitTotal = blueUnitTotal
        missionStats.unitTotal = unitTotal
        missionStats.unitTotalDeaths = unitTotalDeaths
        missionStats.redUnitTotalDeaths = redUnitTotalDeaths
        missionStats.blueUnitTotalDeaths = blueUnitTotalDeaths
        missionStats.redAirbaseTotal = redAirbaseTotal
        missionStats.blueAirbaseTotal = blueAirbaseTotal
        missionStats.redAirplaneUnitTotal = redAirplaneUnitTotal
        missionStats.blueAirplaneUnitTotal = blueAirplaneUnitTotal
        missionStats.airplaneUnitTotal = airplaneUnitTotal
        missionStats.redGroundUnitTotal = redGroundUnitTotal
        missionStats.blueGroundUnitTotal = blueGroundUnitTotal
        missionStats.groundUnitTotal = groundUnitTotal
        missionStats.redShipUnitTotal = redShipUnitTotal
        missionStats.blueShipUnitTotal = blueShipUnitTotal
        missionStats.shipUnitTotal = shipUnitTotal
        missionStats.redHelicopterUnitTotal = redHelicopterUnitTotal
        missionStats.blueHelicopterUnitTotal = blueHelicopterUnitTotal
        missionStats.helicopterUnitTotal = helicopterUnitTotal
        missionStats.playerTotalDeaths = playerTotalDeaths

        jsonMissionStatsData = json:encode(missionStats)
        tcp:send("SM|" ..jsonMissionStatsData.."\n")
        socket.sleep(0.001)

        --[[
        playerTotal, coalitionTotal, players = client_count()

        playerTotals.sessionId = sessionId
        playerTotals.serverId = serverId
        playerTotals.playerTotal = playerTotal
        playerTotals.blueTotal = coalitionTotal.blue
        playerTotals.redTotal = coalitionTotal.red

        env.info("Player Total: " .. playerTotal)
        env.info("Blue Player Total: " .. coalitionTotal.blue)
        env.info("Red Player Total: " .. coalitionTotal.red)

        jsonPlayerTotalsData = json:encode(playerTotals)
        tcp:send("PT|" ..jsonPlayerTotalsData.."\n")
        socket.sleep(0.001)
        --env.info("Players Table: " .. tableDump(players))

        for player, playerData in pairs(players) do
            playerList.serverId = serverId
            playerList.sessionId = sessionId
            playerList.PlayerName = playerData.playerName
            playerList.PlayerCoalition = playerData.playerCoalition

            jsonPlayerListData = json:encode(playerList)
            tcp:send("HP|" ..jsonPlayerListData.."\n")
            socket.sleep(0.001)
        end
        --]]
    end, {}, 15, 300
)

local playerTotals = SCHEDULER:New( nil,
    function()
        playerTotal, coalitionTotal, players = client_count()

        playerTotals.sessionId = sessionId
        playerTotals.serverId = serverId
        playerTotals.playerTotal = playerTotal
        playerTotals.blueTotal = coalitionTotal.blue
        playerTotals.redTotal = coalitionTotal.red

        env.info("Player Total: " .. playerTotal)
        env.info("Blue Player Total: " .. coalitionTotal.blue)
        env.info("Red Player Total: " .. coalitionTotal.red)

        jsonPlayerTotalsData = json:encode(playerTotals)
        tcp:send("PT|" ..jsonPlayerTotalsData.."\n")
        socket.sleep(0.001)
        --env.info("Players Table: " .. tableDump(players))

        for player, playerData in pairs(players) do
            playerList.serverId = serverId
            playerList.sessionId = sessionId
            playerList.PlayerName = playerData.playerName
            playerList.PlayerCoalition = playerData.playerCoalition

            jsonPlayerListData = json:encode(playerList)
            tcp:send("HP|" ..jsonPlayerListData.."\n")
        end
    end, {}, 10, 120
)

--serverStatus.numPlayers = totalPlayerCount
local heartbeatCheckIn = SCHEDULER:New( nil,
    function()
        heartbeat.serverId = serverId
        jsonHeartbeatData = json:encode(heartbeat)
        tcp:send("HU|"..jsonHeartbeatData.."\n")
    end, {}, 5, 60
)